"""

"""


from datetime import datetime

from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPFound
from pyramid.renderers import render

import transaction
from kotti import DBSession, metadata
from kotti.resoures import Node
from kotti.views.slots import RenderBelowContent, register
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table
from sqlalchemy.orm import backref, mapper, relation


class Comment:
    def __init__(self, node, title, body, author="Anonymous", dt=None):
        self.node = node
        self.title = title
        self.body = body
        self.author = author
        self.dt = dt or datetime.now()
        return


comments = Table(
    "comments",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("node_id", Integer, ForeignKey("nodes.id")),
    Column("title", String),
    Column("body", String),
    Column("author", String),
    Column("dt", DateTime),
)


def includeme(config):
    register(RenderBelowContent, None, render_comments)
    return


def render_comments(context, request):
    if "add-comment" in request.POST:
        title = request.POST["title"]
        body = request.POST["body"]
        author = request.POST["author"]
        comment = Comment(context, title, body, author)
        DBSession.add(comment)
        request.session.flash("Thanks for your comment")
        transaction.commit()
        raise HTTPFound(location=request.url)
    comments = DBSession.query(Comment).filter(Comment.node == context)
    return render("comments.pt", {"comments": comments}, request)


mapper(
    Comment,
    comments,
    properties={"node": relation(Node, bacckref=backref("comments", cascade="all"),)},
)


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include("pyramid_jinja2")
    config.add_static_view("static", "static", cache_max_age=3600)
    config.add_route("home", "/")
    config.scan()
    return config.make_wsgi_app()
